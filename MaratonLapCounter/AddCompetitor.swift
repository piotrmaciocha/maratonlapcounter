//
//  AddCompetitor.swift
//  MaratonLapCounter
//
//  Created by Piotr Maciocha on 26.02.2017.
//  Copyright © 2017 Piotr Maciocha. All rights reserved.
//

import UIKit

protocol DataSentDelegate {
    func userDidEnterNewCompetitor(text: String)
}

class AddCompetitor: UIViewController {

    var delegate: DataSentDelegate? = nil
    
    @IBOutlet weak var textForNameOfCompetitor: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    @IBAction func addBtnWasPressed(_ sender: UIButton) {
        if delegate != nil {
            if textForNameOfCompetitor.text != "" {
                let data = textForNameOfCompetitor.text
                delegate?.userDidEnterNewCompetitor(text: data!)
                _ = navigationController?.popViewController(animated: true)
            }
        }
    }
    
}
