//
//  CompetitorListTableViewController.swift
//  MaratonLapCounter
//
//  Created by Piotr Maciocha on 26.02.2017.
//  Copyright © 2017 Piotr Maciocha. All rights reserved.
//

import UIKit

class CompetitorListTableViewController: UITableViewController, DataSentDelegate {
    
    var dataArray = ["Kacper Maciocha", "Grzegorz Kiel", "Karolina Maciocha", "Franciszek Brzeczyszykiewicz"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    //how many section do we have
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1 // to have one kind of grouping all data
    }
    
    //data array designate amount of rows in one section
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataArray.count
    }
    
    
    // what we should display in each of these rows
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //we need to create cell, we need to instatiate it, what is going to display to particular piece of text
        let cell = tableView.dequeueReusableCell(withIdentifier: "CellCompetitor", for: indexPath)
        
        cell.textLabel?.text = dataArray[indexPath.row]        // Configure the cell...
        
        return cell
    }
    
    func userDidEnterNewCompetitor(text: String) {
        dataArray.append(text)
        self.tableView.reloadData()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showAddCard" {
            let vc: AddCompetitor = segue.destination as! AddCompetitor
            vc.delegate = self
        }
    }
    
    
}
